%define name 	gds-dmt
%define version 2.19.7
%define release 1.1
%define daswg   /usr
%define prefix  %{daswg}
%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

Name: 		%{name}
Summary: 	gds-dmt 2.19.7
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	https://software.igwn.org/lscsoft/source//%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gds-base-devel >= 2.19.8
BuildRequires:  gds-frameio-devel >= 2.19.6
BuildRequires:  gds-gui-devel >= 2.19.7
BuildRequires:  gzip bzip2 libXpm-devel
BuildRequires:  ldas-tools-framecpp-devel >= 2.5.8
BuildRequires:  ldas-tools-al-devel
BuildRequires:  root
BuildRequires:  readline-devel fftw-devel
BuildRequires:  jsoncpp-devel
Requires:       gds-frameio-base >= 2.19.6
Requires:       gds-services >= 2.19.8
Requires:       jsoncpp-devel
Prefix:		      %prefix

%description
GDS DMT monitor programs

%package base
Summary: 	Root wrappers for gds libraries
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       gds-frameio-base >= 2.19.6
Requires:       gds-services >= 2.19.8
Requires:       jsoncpp-devel

%description base
GDS DMT monitor programs

%package root
Summary: 	Root wrappers for gds libraries
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: gds-frameio-base >= 2.19.6
Requires: gds-base-headers >= 2.19.8
Requires: gds-lsmp >= 2.19.8

%description root
Root wrappers for gds class libraries and macros/classes for inteactive use
of gds libraries via root.

%package monitors
Summary: 	DMT Monitor programs
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name} = %{version}-%{release}
Requires: gds-base >= 2.19.8
Requires: gds-frameio-base >= 2.19.6
Requires: gds-services >= 2.19.8
Requires: jsoncpp-devel

%description monitors
GDS DMT monitor programs

%package runtime
Summary: 	DMT run-time software
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name}-monitors = %{version}-%{release}

%description runtime
DMT run-time supervisor and services

%package headers
Summary: 	GDS DMT monitor header files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: gds-base-headers >= 2.19.8

%description headers
GDS DMT monitor header files.

%package devel
Summary: 	GDS DMT monitor development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: gds-base-devel >= 2.19.8
Requires: %{name}-headers = %{version}-%{release}

%description devel
GDS DMT monitor  development files.

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure  --prefix=%prefix --libdir=%{_libdir} \
	           --includedir=%{prefix}/include/gds \
	           --enable-online --enable-dtt
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files

%files base
%defattr(-,root,root)
%{_libdir}/libcalcengn.so.*

%files root
%defattr(-,root,root)
%{_datadir}/gds/macros
%{_datadir}/gds/.rootrc
%{_datadir}/gds/root-version

%files monitors
%defattr(-,root,root)
%{_bindir}/DMTGen
%{_libdir}/libdmtenv.so.*
%{_libdir}/libezcalib.so.*
%{_libdir}/libgenerator.so.*
%{_libdir}/liblscemul.so.*
%{_libdir}/libosc.so.*
%{_libdir}/libgds_plotpkg.so.*

%files runtime
%defattr(-,root,root)

%files headers
%defattr(-,root,root)
%{_includedir}/gds

%files devel
%defattr(-,root,root)
%{_libdir}/lib*.a
%{_libdir}/lib*.so
%{_libdir}/pkgconfig

%changelog
* Fri Jan 28 2022 Edward Maros <ed.maros@ligo.org> - 2.19.7-1
- Updated for release as described in NEWS.md

* Fri May 07 2021 Edward Maros <ed.maros@ligo.org> - 2.19.6-1
- Updated for release as described in NEWS.md

* Tue Feb 23 2021 Edward Maros <ed.maros@ligo.org> - 2.19.5-1
- Removed references to gds-base-crtools package
- Corrected Debian buster packaging

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.4-1
- Removed python configuration
- Modified Source field of RPM spec file to have a fully qualified URI

* Tue Feb 02 2021 Edward Maros <ed.maros@ligo.org> - 2.19.3-1
- Corrections to build rules

* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Split lowlatency as a separate package

* Thu Jun 11 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Removed FrameL
- Moved DMTifc to obsolete
